import resource
#IDENTIFIANTS DE L'EXPERIENCE

# ID of xp
num_xp='xp_framework'

#Path to save the experience and the objects created
PATHTODATA='/data/'
# CREATE SETS
# Past/Future limit in the experiments

t_val ='2015-04-26 23:59:59'
t_max = '2015-04-30 23:59:59'
t_test= '2015-05-07 23:59:59'
# Ratio of undersampling in Random forest training
genuine_over_fraud_ratio = 9

# ratio of remaining genuine sequencies to train HMM (can be number)
remaining_genuine_to_train_HMM = 0.04

nb_genuine_term_HMM = 5000

# minimal number of transactions in the train period for a sequency to be used
tx_in_past = 3
window_min=3

# ANOMALY DETECTION
n_iter = 30

# RANDOM FOREST

# list of features used in random forest

feat_rf = ['newPanID','TERM_MIDUID','TX_DATETIME','TERM_MCC','TERM_COUNTRY','TX_AMOUNT','TX_EMV','TX_INTL',
           'LANGUAGE','CARD_BRAND','COUNTRY','PROVINCE_CODE','DISTRICT_CODE','INS_CODE','ZIP','CITY','CARD_TRAVEL_KEY','TX_LOCAL_CURRENCY',
           'TX_PROCESS','TX_3D_SECURE', 'DT_wday', 'DT_hour','TX_INTL','AGE','GENDER','BROKER','CARD_EXPIRY'
    ,'CARD_TYPE','CARD_AUTHENTICATION', 'TX_CARD_ENTRY_MODE','TX_ECOM_IND','TX_FRAUD','TX_LOCAL_AMOUNT']

feat_rf += ['tdelta']
feat_rf += ['aggCH1', 'aggCH2', 'aggCH3', 'aggCH4']
feat_rf += ['aggTM1', 'aggTM2', 'aggTM3', 'aggTM4']
feat_rf += ['3_5_amount_CH_fraud',
            '3_5_amount_CH_genuine',
            '3_5_amount_TM_fraud',
            '3_5_amount_TM_genuine',
            '3_5_tdelta_CH_fraud',
            '3_5_tdelta_CH_genuine',
            '3_5_tdelta_TM_fraud',
            '3_5_tdelta_TM_genuine']

use_cols={'TERM_MCC':'object','TERM_COUNTRY':'object','TX_AMOUNT':'float32','TX_INTL':'bool','AGE':'float32',
          'TX_LOCAL_AMOUNT':'float32','GENDER':'object','BROKER':'object','CARD_TYPE':'object','CARD_AUTHENTICATION':'object',
          'TX_CARD_ENTRY_MODE':'object','CREDIT_LIMIT':'float32','TX_DATETIME':'object', 'TX_FRAUD':'bool',
          'newPanID':'object', 'TERM_MIDUID':'object', 'TX_3D_SECURE':'object','TX_ECOM_IND':'object','LANGUAGE':'object',
          'CARD_BRAND':'object','COUNTRY':'object','PROVINCE_CODE':'object','DISTRICT_CODE':'object','INS_CODE':'object',
          'ZIP':'object','CITY':'object','CARD_TRAVEL_KEY':'bool','TX_EMV':'object','TX_LOCAL_CURRENCY':'object',
          'TX_PROCESS':'object','CARD_EXPIRY':'float32'}

#use_cols={'TERM_MCC':'object','TERM_COUNTRY':'object','TX_AMOUNT':'float32','TX_INTL':'float32','AGE':'float32','TX_LOCAL_AMOUNT':'float32',
#          'GENDER':'object','BROKER':'object','CARD_TYPE':'object','CARD_AUTHENTICATION':'object', 'TX_CARD_ENTRY_MODE':'object','CREDIT_LIMIT':'float32',
#             'TX_DATETIME':'object', 'TX_FRAUD':'object', 'newPanID':'object', 'TERM_MIDUID':'object', 'TX_3D_SECURE':'object','TX_ECOM_IND':'object'
#    ,'LANGUAGE':'object','CARD_BRAND':'object','COUNTRY':'object','PROVINCE_CODE':'object','DISTRICT_CODE':'object','INS_CODE':'object','ZIP':'object','CITY':'object',
#    'CARD_TRAVEL_KEY':'object','TX_EMV':'object','TX_LOCAL_CURRENCY':'object','TX_PROCESS':'object','CARD_EXPIRY':'float32'}
"""
e-commerce feature: ['TERM_MCC', 'TERM_COUNTRY', 'TX_AMOUNT','TX_3D_SECURE', 'TX_PROCESS'
            'TX_ECOM_IND', 'DT_wday', 'DT_hour','TX_INTL',
           'AGE','GENDER','BROKER','CARD_TYPE','CARD_AUTHENTICATION', 'TX_CARD_ENTRY_MODE',
           'HMM_score_fraud','HMM_score_genuine', 'HMM_term_fraud','HMM_term_genuine']

f2f feature: ['TERM_MCC', 'TERM_COUNTRY', 'TX_AMOUNT','TX_EMV', 'DT_wday', 'DT_hour','TX_INTL',
           'AGE','GENDER','BROKER','CARD_TYPE','CARD_AUTHENTICATION', 'TX_CARD_ENTRY_MODE',
           'HMM_score_fraud','HMM_score_genuine','HMM_term_fraud','HMM_term_genuine']
Deleted features
{newPanID','TERM_MIDUID','TX_ECOM','TX_3D_SECURE','TX_EMV','LANGUAGE','CARD_BRAND',
            'CARD_EXPIRY', 'ZIP', 'CITY','COUNTRY','PROVINCE_CODE','DISTRICT_CODE',
            'INS_CODE','CARD_TRAVEL_KEY','TX_LOCAL_CURRENCY','TX_PROCESS','TX_ECOM_IND'}
"""

gap_of_1_week = False

"""
RELATIVE PATHS WHERE THE OBJECTS CREATED DURING EXPERIMENT WILL BE STORED
- The origin path is PATHTODATA
"""
raws = PATHTODATA + 'datasets/ECOM/'
PATHTOXP = PATHTODATA + 'experiences/' + str(num_xp) + '/'


python_objects = PATHTOXP + 'python_objects/'
figures = PATHTOXP + 'figures/'
genuinetrainrfvqscore = PATHTOXP + 'rf/genuine/'
fraudulenttrainrfvqscore = PATHTOXP + 'rf/compromised/'
testfinal = PATHTOXP + 'test_new/'
testfinalbis = PATHTOXP + 'testfinalbis/'


results = PATHTOXP + 'pred_/'

savelog = PATHTODATA + 'experiences/' + str(num_xp) + '/log_' + \
          str(num_xp) + '.txt'  # path to log file
error = PATHTODATA + 'experiences/' + str(num_xp) + '/errorlog_' + \
        str(num_xp) + '.txt'  # path to log file

num_info = python_objects + 'num_info_dic'
encoders = python_objects + 'encoders'
one_hot = python_objects + 'one-hot'
genuine_hmm = python_objects + 'genuine_hmm'
fraudulent_hmm = python_objects + 'fraudulent_hmm'
hmm_term_genuine = python_objects + 'hmm_term_genuine'
hmm_term_compromised = python_objects + 'hmm_term_compromised'
train_rf = python_objects + 'train_rf.pkl'

genuinetrainrf = python_objects + 'ls_rf_genuine'
genuinetrainhmm = python_objects + 'ls_hmm_genuine'
fraudulenttrainrf = python_objects + 'ls_rf_compromised'
genuinetermhmm = python_objects + 'ls_term_genuine'
compromisedtermhmm = python_objects + 'ls_term_compromised'


"""
Small useful functions for more comprehension when reading code
"""

from time import gmtime, strftime


import pandas as pd
def very_deep_copy(self):
    return pd.DataFrame(self.values.copy(), self.index.copy(), self.columns.copy())

def log(message):
    with open(savelog, 'a') as logger:
        logger.write(strftime("%H:%M:%S", gmtime()) + ' ' + message)
    print message
    return

def change_extension(filename, new_extension):
    if filename[-4] == '.':
        return filename[:-4] + new_extension
    else:
        print 'Wrong filename format : ', filename


def date_from_filename(filename):
    if filename[-4] == '.':
        return int(filename[-12:-4])
    else:
        print 'Wrong filename format : ', filename


def train_test_limit():
    return int(t_max[0:4] + t_max[5:7] + t_max[8:10])


def config_parameters(saveconfig):
    dic = globals()
    with open(saveconfig + 'config_parameters.txt', 'w') as save:
        ls_key = dic.keys()
        ls_key.sort()
        for key in ls_key:
            if '__' not in key:
                save.write(str(key) + ' : ' + str(dic[key]) + '\n')

